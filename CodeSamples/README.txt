William Rockwell

Code Samples can be found in the titular folder
There are 2 samples in there, one for a linked list and one containing a .cpp and .h file for a character made in Unreal Engine

The Linked List sample was made back in Fall of 2018. The user enters how many players there are, and the code then distributes cards to every player.
The logic behind these cards is driven by linked lists. It was made as a part of the final assignment for a programming course I took.

The Action Sample contains a .cpp and .h file made for Unreal Engine in the Fall of 2021. They contain the logic of the player character, 
so input and output is done via the game engine.
It was made for an independent study course, with the purpose of familiarizing myself with Unreal.