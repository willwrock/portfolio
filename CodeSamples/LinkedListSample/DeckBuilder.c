//William Rockwell
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
typedef struct card_s { //Card Structure
	char CardValue[3]; //String for the cards value
	char suit[7];
	int value;
	struct card_s *next;
}card;
typedef struct player_s { //Player Structure
	char Name[100];
	card *Hand;
	struct player_s *next;
}player;
int NodeCount(card *HN) { //Tally's up how many nodes there are
	int i = 0;
	card *tmp;
	tmp = HN;
	while (tmp != NULL) {
		i++;
		tmp = tmp->next;
	}
	return(i);
}
void printDeck(card *Deck) {
	card *tmp = Deck;
	while (tmp != NULL) {
		printf("%s\n", tmp->CardValue);
		tmp = tmp->next;
	}
}
card * return_nth_node(card *HN, int N) { //returns a node value
	card *tmp;
	tmp = HN;
	int i = 1, L;

	L = NodeCount(HN);
	if (N > L) { //Failsafe in case user puts in a value greater than the number of nodes
		N = L;
	}
	while (i < N) { //increase pointer value until desired node is reached
		i++;
		tmp = tmp->next;
	}
	return(tmp);
}
card * remove_nth_node(card *HN, int N) { //Removes the given Nth node
	card* tmp = NULL, * tmp2 = NULL, *Return_This_Node = (card*)calloc(sizeof(card), 1);

	if (N == 1) {
		Return_This_Node = HN;
		HN = HN->next;
		return(HN);

	}
	else {
		tmp = return_nth_node(HN, N-1);
		Return_This_Node = tmp->next;
		tmp->next = Return_This_Node->next;
	}
	Return_This_Node->next = NULL;
	return(Return_This_Node);
}

void add_nth_node(card **HN, card *AddThisNode, int N) { //Adds a node
	card *tmp = NULL;

	if (N == 1) {
		AddThisNode->next = *HN;
		*HN = AddThisNode;
	}
	else {
		tmp = return_nth_node(*HN, N - 1);
		AddThisNode->next = tmp->next;
		tmp->next = AddThisNode;
	}
	return;
}

void print_nth_node(card *HN, int N) { //Print Function for type card
	card *tmp;
	tmp = HN;
	int i = 1;

	while (i < N) { //Increase pointer value until it hits the desired pointer
		i++;
		tmp = tmp->next;
	}
	printf("%s\n", tmp->CardValue);
	return;
}
void print_nth_node_player(player *HN, int N) { //Print Function for type player
	player *tmp;
	tmp = HN;
	int i = 1;

	while (i < N) { //Increase pointer value until it hits the desired pointer
		i++;
		tmp = tmp->next;
	}
	printf("%s\n", tmp->Name);
	return;
}

void append(card *HN, card *Value) { //Adds a node and it's value to the head of a linked list
	Value->next = HN->next;
	HN->next = Value;	
}

card * draw_random_card(card *DeckCard, card *Card, int x) { //Removes a card from the deck, adds it to a players hand 
	card* tmp;
	card* tmp2, *tmp3;
	tmp = DeckCard;
	tmp2 = (card*)calloc(sizeof(card), 1);
	tmp3 = NULL;
	int i = 1;
	int Length;
	Length = NodeCount(DeckCard);
	while (i < x) { //Increase pointer value until it hits the desired pointer
		i++;
		tmp = tmp->next;
	}	
	
	if (i == 1) { //Head node special case
		tmp2 = return_nth_node(DeckCard, i);
		DeckCard = remove_nth_node(DeckCard, i);
		tmp2->next = NULL;
	}
	else {
		tmp2 = remove_nth_node(DeckCard, i);
	}

	if (Card->CardValue[0] == NULL) { //If we're dealing with a node without any data in it, assign card to the node
		strcpy(Card->CardValue, tmp2->CardValue);
	}
	else { //Else, Append to the list
		append(Card, tmp2);
	}
	return(DeckCard);
}

int main(void) {
	card *Deck, *tmp, *tmp2;
	player *PlayerCards, *tmp3;
	char playername[100];
	int numPlayers = 0;
	int i, L;
	char headvalue[] = "1\x03";
	int j = 1, n = 5, x;
	time_t t;
	int numCards = 51;
	srand((unsigned)time(&t)); //implement a seed

	Deck = (card *)calloc(sizeof(card), 1); //The head of the deck
	PlayerCards = (player *)calloc(sizeof(player), 1); //The head of players list
	PlayerCards->Hand = (card *)calloc(sizeof(card), 1); //The head of the first players hand

	Deck->suit[0] = '\x03'; //Heart 
	Deck->suit[1] = '\x04'; //Diamond
	Deck->suit[2] = '\x05'; //Club
	Deck->suit[3] = '\x06'; //Spade
	Deck->suit[4] = 'J'; //Joker
	Deck->suit[5] = 'Q'; //Queen
	Deck->suit[6] = 'K'; //King

	strcpy(Deck->CardValue, headvalue); //Set head as 1 of hearts

	tmp = Deck;
	//Initializing the Deck
	for (i = 1; i < 4; i++) { //One while loop that will fill in all 1's with the 7 card characters
		tmp->next = (card *)calloc(sizeof(card), 1);
		tmp = tmp->next;
		sprintf(tmp->CardValue, "%d%c", j, Deck->suit[i]);
	}
	j += 1;

	//A nested loop that will loop through each individual number and it's card character combinations, starting at 2
	while (j < 11) {
		for (i = 0; i < 4; i++) {
			tmp->next = (card *)calloc(sizeof(card), 1);
			tmp = tmp->next;
			sprintf(tmp->CardValue, "%d%c", j, Deck->suit[i]);
		}
		j += 1;
	}
	//Another nested loop for matching remaining cards
	for (i = 0; i < 4; i++) { //i represents the club, heart, diamond, and spade
		for (j = 4; j < 7; j++) { //j represents the king, queen, and jack
			tmp->next = (card *)calloc(sizeof(card), 1);
			tmp = tmp->next;
			sprintf(tmp->CardValue, "%c%c", Deck->suit[j], Deck->suit[i]);
		}
	} //End of Deck initialization

	printDeck(Deck);
	printf("Enter the number of players: ");
	scanf("%d", &numPlayers);

	tmp3 = PlayerCards;
	fgetc(stdin);
	for (i = 1; i <= numPlayers; i++) { //Take in Players names
		printf("Player number %d is: ", i);
		
		fgets(playername, 100, stdin);
		L = strlen(playername);
		playername[L - 1] = '\0';
		strcpy(tmp3->Name, playername);
		tmp3->next = (player *)calloc(sizeof(player), 1); //Allocate space for next player
		tmp3 = tmp3->next;
		tmp3->Hand = (card *)calloc(sizeof(card), 1); //Allocate space for the heads of every players hand
	}
	tmp3 = PlayerCards;

	//Draw everyones beginning hand
	if (numPlayers < 5) {	//7 cards are dealt if there are 4 or fewer players
		for (j = 0; j < numPlayers; j++) {
			for (i = 0; i < 7; i++) { //Draw cards for one person
				x = (rand() % numCards + 1); // +1 so it doesn't draw from zero
				Deck = draw_random_card(Deck, tmp3->Hand, x);
				numCards -= 1;
			}
			tmp3 = tmp3->next; //Move onto next person
		}
	}
	else {     //5 if more than 4
		for (j = 0; j < numPlayers; j++) {
			for (i = 0; i < 5; i++) { //Draw cards for one person
				x = (rand() % numCards);
				Deck = draw_random_card(Deck, tmp3->Hand, x);
				numCards -= 1;
			}
			tmp3 = tmp3->next; //Move onto the next person
		}
	}
	tmp3 = PlayerCards;
	for (j = 1; j <= numPlayers; j++) { //Prints names and all their cards
		print_nth_node_player(PlayerCards, j);
		printDeck(tmp3->Hand);
		tmp3 = tmp3->next;
	}
	printf("DECK\n");	
	printDeck(Deck); //Print all remaining cards in the deck
}