// Fill out your copyright notice in the Description page of Project Settings.
#include "ActionPlayer.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components//BoxComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/DataTable.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Enemy.h"
#include "Fireball.h"
#include "Particles/ParticleSystemComponent.h"
#include "Particles/ParticleSystem.h"
#include "PhysicsEngine/RadialForceComponent.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"

// Sets default values
AActionPlayer::AActionPlayer()
{
	PrimaryActorTick.bCanEverTick = true;

	Staff = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Staff"));
	Staff->SetupAttachment(GetMesh());

	MovementComponent = (UCharacterMovementComponent*)GetCharacterMovement();
	BaseWalkSpeed = MovementComponent->MaxWalkSpeed;

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraAttachmentArm"));
	SpringArm->SetupAttachment(RootComponent);

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("ActualCamera"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);

	FireParticlesComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FireParticleComp"));
	FireParticlesComp->SetupAttachment(Staff);

	ManaComp = CreateDefaultSubobject<UNiagaraComponent>(TEXT("ManaParticleComp"));
	ManaComp->SetupAttachment(Staff);

	DashBoostParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("DashPowerComp"));
	DashBoostParticles->SetupAttachment(Staff);

	HealParticlesComp = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("HealParticleComp"));
	HealParticlesComp->SetupAttachment(Staff);

	//Load animation montage
	static ConstructorHelpers::FObjectFinder<UDataTable> MeleeAttackDTObject(TEXT("DataTable'/Game/Animations/Mine/Melee/PlayerActions.PlayerActions'"));
	if (MeleeAttackDTObject.Succeeded()) {
		AttackDT = MeleeAttackDTObject.Object;
	}
	TeamId = FGenericTeamId(1);	

	StaffCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("StaffCollisonBox"));
	StaffCollision->SetupAttachment(Staff);
	StaffCollision->SetCollisionProfileName(MeleeCollisionProfile.Disabled);

	EnemyDetectionCollision = CreateDefaultSubobject<USphereComponent>(TEXT("EnemySphere"));
	EnemyDetectionCollision->SetupAttachment(RootComponent);
	
	ExplosiveComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("Explosive force component"));
	ExplosiveComponent->SetupAttachment(Staff);
	ExplosiveComponent->DestructibleDamage = MagicPower;

	CraterExplosion = CreateDefaultSubobject<URadialForceComponent>(TEXT("CraterExplosive"));
	CraterExplosion->SetupAttachment(RootComponent);
	CraterExplosion->DestructibleDamage = MagicPower;
	
}

void AActionPlayer::BeginPlay()
{
	Super::BeginPlay();
	baseJumpVelocity = MovementComponent->JumpZVelocity;
	CanDash = true;
	CurrentState = Normal;
	HP = MaxHP;
	//Mana = MaxMana;
	HealthPercent = HP / MaxHP;

	DashPercent = TimeManager.DashTimer / DashCountdownTime;
	ManaPercent = Mana / MaxMana;
	DashBoost = false;

	Staff->AttachToComponent(GetMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, EAttachmentRule::SnapToTarget, EAttachmentRule::KeepWorld, false), "hand_rSocket");
	//StaffCollision->SetHiddenInGame(false);

	//This only works if you set weapon and enemy to block each other in project settings collision
	StaffCollision->OnComponentHit.AddDynamic(this, &AActionPlayer::OnAttackHit);
	StaffCollision->SetNotifyRigidBodyCollision(false);

	//This only works if you set weapon and enemy to overlap each other in project settings collision
	StaffCollision->OnComponentBeginOverlap.AddDynamic(this, &AActionPlayer::OnAttackOverlapBegin);
	StaffCollision->OnComponentEndOverlap.AddDynamic(this, &AActionPlayer::OnAttackOverlapEnd);
	StaffCollision->SetGenerateOverlapEvents(false);

	EnemyDetectionCollision->OnComponentBeginOverlap.AddDynamic(this, &AActionPlayer::EnemyInRange);
	EnemyDetectionCollision->OnComponentEndOverlap.AddDynamic(this, &AActionPlayer::EnemyOutOfRange);
	EnemyDetectionCollision->SetGenerateOverlapEvents(true);

	DodgeDirection = Back; //Set defaults
	AttackType = Nothing;
	AttackMode = Melee;
	FireParticlesComp->Deactivate();
	HealParticlesComp->Deactivate();
	DashBoostParticles->Deactivate();
	QueuedAttack = false;
	IgnoredActors.Add(this);
	LightAttackIndex = 0;
	HeavyAttackIndex = 0;
	FString ContextString(TEXT("Player Attack Montage Context"));
	FString DamageString(TEXT("Player Damage Montage Context"));
	FString DodgeString(TEXT("Player Dodge Montage Context"));

	DamageMontage = AttackDT->FindRow<FPlayerActions>(FName(TEXT("Damage1")), DamageString, true);
	TimeManager.AttackTimer = AttackCountdownTime;
	TimeManager.InvincibilityTimer = InvincibilityCountdownTime;
	TimeManager.DashTimer = DashCountdownTime;
	TimeManager.HealTimer = HealCountdownTime;
	BaseFriction = MovementComponent->GroundFriction;
}

// Called every frame
void AActionPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (TargetLocked) {
		TargetFocus();
	}
	if (ManaComp) {
		ManaComp->SetNiagaraVariableVec3("User.AttractorPosition", GetActorLocation()); //Set vector location so niagara particles will always track the players location 
	}
}

// Called to bind functionality to input
void AActionPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AActionPlayer::StartJump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AActionPlayer::StopJump);

	PlayerInputComponent->BindAction("Slow", IE_Pressed, this, &AActionPlayer::StartSlowWalk);
	PlayerInputComponent->BindAction("Slow", IE_Released, this, &AActionPlayer::StopSlowWalk);

	PlayerInputComponent->BindAction("Pray", IE_Pressed, this, &AActionPlayer::StartHealing);
	PlayerInputComponent->BindAction("Pray", IE_Released, this, &AActionPlayer::StopHealing);

	PlayerInputComponent->BindAction("Dive", IE_Pressed, this, &AActionPlayer::Dash);
	PlayerInputComponent->BindAction("LightAttack", IE_Pressed, this, &AActionPlayer::LightAttackInput);
	PlayerInputComponent->BindAction("HeavyAttack", IE_Pressed, this, &AActionPlayer::HeavyAttackInput);
	PlayerInputComponent->BindAction("Switch", IE_Pressed, this, &AActionPlayer::SwitchMode);
	PlayerInputComponent->BindAction("Dodge", IE_Pressed, this, &AActionPlayer::Dodge);	
	PlayerInputComponent->BindAction("Restart", IE_Pressed, this, &AActionPlayer::ReloadLevel);
	PlayerInputComponent->BindAction("Exit", IE_Pressed, this, &AActionPlayer::QuitFunction);

	PlayerInputComponent->BindAxis("MoveForward", this, &AActionPlayer::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AActionPlayer::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AActionPlayer::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AActionPlayer::LookUpAtRate);
}

void AActionPlayer::MoveForward(float Value) {
	if ((Controller != NULL) && (Value != 0.0f))
	{	
		if (Value < 0) {//Keep track of what direction the player is moving for dodge
			DodgeDirection = Back;
		}
		else if (Value > 0) {
			DodgeDirection = Fore;
		}
		if (CurrentState != Attacking && CurrentState != InDodge && CurrentState != Damaged) {
			
			const FRotator Rotation = Controller->GetControlRotation();// find out which way is forward
			const FRotator YawRotation(0, Rotation.Yaw, 0);		
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X); // get forward vector
			AddMovementInput(Direction, Value);
		}
	}
}

void AActionPlayer::MoveRight(float Value) {
	if ((Controller != NULL) && (Value != 0.0f))
	{
		if (Value < 0) { //Keep track of what direction the player is moving for dodge
			DodgeDirection = Left;
		}
		else if (Value > 0) {
			DodgeDirection = Right;
		}
		if (CurrentState != Attacking && CurrentState != InDodge && CurrentState != Damaged) {			
			const FRotator Rotation = Controller->GetControlRotation();// find out which way is right
			const FRotator YawRotation(0, Rotation.Yaw, 0);			
			const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);// get right vector 			
			AddMovementInput(Direction, Value);// add movement in that direction
		}
	}
}

void AActionPlayer::Dash() {
	if (CanDash && CurrentState != InDodge && CurrentState != Damaged && CurrentState != Dead) {
		if (Mana >= LightSpellCost) {
			ResetAttackState();
			MovementComponent->GroundFriction = DashFriction;
			MovementComponent->MaxWalkSpeed *= DashSpeed;

			MovementComponent->AddImpulse(GetActorForwardVector() * DashPower, true);
			CanDash = false;
			DashBoost = true;
			DashBoostParticles->Activate();
			LoseMana(5);
			BeginCountDown(TimeManager.Dash);
		}
	}
}

void AActionPlayer::ReloadLevel() {
	UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
}

void AActionPlayer::QuitFunction() {
	UKismetSystemLibrary::QuitGame(GetWorld(), (APlayerController*)GetController(), EQuitPreference::Quit, false);
}

void AActionPlayer::Dodge() {
	if (MovementComponent->IsMovingOnGround() && CurrentState != Damaged && CurrentState != InDodge && CurrentState != Dead) {
		FRotator NewRotation = Camera->GetForwardVector().Rotation();
		NewRotation.Pitch *= 0;
		FVector DodgeVector = Camera->GetForwardVector(); //Determine direction of movement based on which way the camera is facing
		DodgeVector.Z *= 0;
		DodgeVector = DodgeVector.GetSafeNormal();
		SetActorRotation(NewRotation);
		ResetAttackState();
		CurrentState = InDodge;		
		if (DodgeDirection == Back) { 
			MovementComponent->AddImpulse(-DodgeVector * DodgePower, true);
		}
		else if (DodgeDirection == Fore) {
			MovementComponent->AddImpulse(DodgeVector * DodgePower, true);
		}
		else if (DodgeDirection == Right) {
			MovementComponent->AddImpulse(FVector::CrossProduct(GetActorUpVector(), DodgeVector) * DodgePower, true);
		}
		else if (DodgeDirection == Left) {
			MovementComponent->AddImpulse(FVector::CrossProduct(DodgeVector, GetActorUpVector()) * DodgePower, true);
		}
	}
}

void AActionPlayer::FocusCamera() {
	FRotator NewRotation = GetActorForwardVector().Rotation();
}

void AActionPlayer::Death() {
	HP = 0;
	ResetAttackState();
	CurrentState = Dead;
	CanDash = false;
	SetActorEnableCollision(false);
}

void AActionPlayer::StartSlowWalk() {
	MovementComponent->MaxWalkSpeed /= 2;
}

void AActionPlayer::StopSlowWalk() {
	MovementComponent->MaxWalkSpeed = BaseWalkSpeed;
}

void AActionPlayer::LookUpAtRate(float Rate) {
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AActionPlayer::TurnAtRate(float Rate) {
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AActionPlayer::Landed(const FHitResult& Hit) {
	Super::Landed(Hit);
	CurrentState = Normal;
	MovementComponent->JumpZVelocity = baseJumpVelocity;
	CanDash = true;
}

void AActionPlayer::LoseHealth(float amount) {
	HP -= amount;
	HealthPercent = HP / MaxHP;
	if (HP <= 0) {
		Death();
	}
}

void AActionPlayer::RestoreHealth(float amount) {
	HP += amount;
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), HealSound, GetActorLocation());
	if (HP > MaxHP) {
		HP = MaxHP;
	}
	HealthPercent = HP / MaxHP;
}

void AActionPlayer::StartHealing() {
	if (HP < MaxHP && MovementComponent->IsMovingOnGround() && CurrentState != Dead) {
		HealParticlesComp->Activate();
		CurrentState = Healing;
		BeginCountDown(TimeManager.Heal);
	}
}

void AActionPlayer::StopHealing() {
	HealParticlesComp->Deactivate();
	CurrentState = Normal;
	GetWorldTimerManager().ClearTimer(TimeManager.HealHandle);
}

void AActionPlayer::LoseMana(float amount) {
	if (DashBoost) {
		Mana -= amount / 2.0f;
	}
	else {
		Mana -= amount;
	}
	
	if (Mana < 0) {
		Mana = 0;
	}
	ManaPercent = Mana / MaxMana;
}

void AActionPlayer::RestoreMana(float amount) {
	bool NotAtMax = false;
	if (Mana < MaxMana) {
		NotAtMax = true;
	}
	if (DashBoost) {
		Mana += amount * 1.25;
	}
	else {
		Mana += amount;
	}
	
	if (Mana >= MaxMana) {
		Mana = MaxMana;
		if (NotAtMax) {
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), ManaParticles, GetActorLocation(), GetActorRotation(), GetActorScale(), true, true, ENCPoolMethod::FreeInPool, false);
		}		
	}
	ManaPercent = Mana / MaxMana;
}

void AActionPlayer::StartJump() {
	if (HP > 0 && CurrentState != Damaged && CurrentState != InDodge && CurrentState != Attacking) {
		CurrentState = InAir;
		bPressedJump = true;
		if (JumpCurrentCount > 0) {
			MovementComponent->JumpZVelocity = DoubleJumpPower;
		}
	}
}

void AActionPlayer::StopJump() {
	bPressedJump = false;
	ResetJumpState();
}

float AActionPlayer::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
	if (!Invincible) {
		GetWorldTimerManager().ClearTimer(TimeManager.HealHandle);
		ResetAttackState();
		CurrentState = Damaged;	
		LoseHealth(DamageAmount);
		if (DamageAmount < 4) {
			PlayAnimMontage(DamageMontage->Montage, 1.0f, FName("Damage1_Start")); //Use montages so that we can enter the animation from any state
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), DamageSounds[0], GetActorLocation());
		}
		else {
			PlayAnimMontage(DamageMontage->Montage, 1.0f, FName("Damage2_Start"));
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), DamageSounds[1], GetActorLocation());
		}
		Invincible = true;	
		FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), DamageCauser->GetTargetLocation()); //Look at enemy for animation simplicity
		NewRotation.Pitch *= 0; //Because we look at the enemy upon taking damage, some damage animations cause the mesh to rotate it, this sets it back to normal
		SetActorRotation(NewRotation);	
	}
	return DamageAmount;
}

void AActionPlayer::MeleeAttack() {
	StaffCollision->SetCollisionProfileName(MeleeCollisionProfile.Enabled);
	StaffCollision->SetNotifyRigidBodyCollision(true); //Simulate Generate Hit Events in Blueprints
	StaffCollision->SetGenerateOverlapEvents(true);
}

void AActionPlayer::MeleeAttackEnd() {
	StaffCollision->SetCollisionProfileName(MeleeCollisionProfile.Disabled);
	StaffCollision->SetNotifyRigidBodyCollision(false);
	StaffCollision->SetGenerateOverlapEvents(false);
	HitActors.Empty();
}

void AActionPlayer::MagicAttackStart() {

}
void AActionPlayer::MagicAttackEnd() {

}

void AActionPlayer::LightAttackInput() {
	AttackType = Light;
	if (MovementComponent->IsMovingOnGround() && CurrentState != Damaged && CurrentState != InDodge && CurrentState != Attacking && CurrentState != Dead) {
		NextAttack();
	}
	else {
		QueuedAttack = true; //If player presses attack button while still in attack, queue up the next action to occur when it can
	}
}

void AActionPlayer::HeavyAttackInput() {
	AttackType = Heavy;
	if (CurrentState != InAir && CurrentState != Damaged && CurrentState != InDodge && CurrentState != Attacking && CurrentState != Dead) {
		NextAttack();
	}
	else {
		QueuedAttack = true; //If player presses attack button while still in attack, queue up the next action to occur when it can
	}
}

void AActionPlayer::NextAttack() {
	IncrementIndex(AttackType);
	CurrentState = Attacking;	
	float ClosestToView = 0; //Look at nearest enemy
	int index = 0;
	float LookAtPrecision;
	if (EnemyArray.Num() > 0) { //Only loop and compare if there's more than 1 enemy in the array
		LookAtPrecision = FVector::DotProduct((EnemyArray[0]->GetActorLocation() - GetActorLocation()).GetSafeNormal(), GetActorForwardVector());
		ClosestToView = LookAtPrecision;
		for (int i = 1; i < EnemyArray.Num(); i++) {
		
			LookAtPrecision = FVector::DotProduct((EnemyArray[i]->GetActorLocation() - GetActorLocation()).GetSafeNormal(), GetActorForwardVector());

			if (LookAtPrecision > ClosestToView) {
				ClosestToView = LookAtPrecision;
				index = i;
			}

		}
		//Should only lock on to targets with a negative dot product if there's no one else to lock on to
		TargetLocked = true;
		CurrentTarget = EnemyArray[index];
	}	
}
//Logic for combo system. Increment the supplied index so the next attack in combo chain will be fired off on attack
void AActionPlayer::IncrementIndex(FAttackType Type) { 
	if (Type == Light) {
		LightAttackIndex += 1;
		if (LightAttackIndex > 5) {
			LightAttackIndex = 1;
		}
	}
	else {
		HeavyAttackIndex += 1;
		if (HeavyAttackIndex > 3) {
			HeavyAttackIndex = 0;
		}
	}
}

void AActionPlayer::SwitchMode() { 
	if (AttackMode == Melee) {
		AttackMode = Magic;
		FireParticlesComp->Activate();
		
		
	}
	else {
		AttackMode = Melee;
		FireParticlesComp->Deactivate();
	}
}

//For when weapon and Enemy block each other(Project Settings)
void AActionPlayer::OnAttackHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {
	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Blue, "Hit Delegate!");
}

//For selecting the enemy to target
void AActionPlayer::FindTarget() {

}

//For Targeting and strafing an enemy
//Instead of Releasing the button, if the target is already locked, unlock the targt (or change targets)
void AActionPlayer::TargetFocus() {
	if (CurrentTarget != NULL) {
		FRotator NewRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CurrentTarget->GetTargetLocation());
		NewRotation.Pitch *= 0;

		SetActorRotation(NewRotation);
	}

}

void AActionPlayer::TargetRelease() {
	TargetLocked = false;
}

//Generate an explosion at tip of staff
void AActionPlayer::Explosion() {
	
	if (Mana >= HeavySpellCost) {
		LoseMana(LightSpellCost);
		FVector SpawnLocation = FireParticlesComp->GetComponentLocation();
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticles, SpawnLocation, GetActorRotation(), true, EPSCPoolMethod::None);
		UGameplayStatics::ApplyRadialDamage(GetWorld(), MagicPower, SpawnLocation, ExplosiveComponent->Radius, UDamageType::StaticClass(), IgnoredActors, NULL, (AController*)GetOwner(), true, ECollisionChannel::ECC_Visibility);
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSound, GetActorLocation());

	}

}
//Catch all for spawning certain magic attacks
void AActionPlayer::SummonMagic(UClass* Class) {
	if (Mana >= LightSpellCost) {
		FVector SpawnLocation = FireParticlesComp->GetComponentLocation();
		FRotator SpawnRotation = GetActorForwardVector().Rotation();
		SpawnRotation.Pitch = 0;
		UWorld* World = GetWorld();
		if (World) {
			
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = this;
			AFireball* Projectile = World->SpawnActor<AFireball>(Class, SpawnLocation, SpawnRotation, SpawnParams);

			if (Projectile) {
				Projectile->OwningCharacter = this;
				FVector LaunchDirection = SpawnRotation.Vector();
				Projectile->FireInDirection(LaunchDirection);
			}
		}
	}
}

//Shoot a fireball from the tip of staff
void AActionPlayer::Fireball() {
	SummonMagic(FireballClass);
	LoseMana(LightSpellCost);
}

void AActionPlayer::SummonCrater() {
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), CraterSound, GetActorLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), CraterParticles, CraterExplosion->GetComponentLocation(), GetActorRotation(), true, EPSCPoolMethod::None);
	UGameplayStatics::ApplyRadialDamage(GetWorld(), MagicPower, CraterExplosion->GetComponentLocation(), CraterExplosion->Radius, UDamageType::StaticClass(), IgnoredActors, NULL, (AController*)GetOwner(), true, ECollisionChannel::ECC_Visibility);
	CraterExplosion->FireImpulse();
	LoseMana(HeavySpellCost);
}

void AActionPlayer::Grenade() {
	SummonMagic(ExplosiveFireballClass);
	LoseMana(HeavySpellCost);
}
//For when weapon and Enemy overlap each other (Project Settings)
void AActionPlayer::OnAttackOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (OtherActor->CanBeDamaged()) {

		//Prevent attack from hitting an enemy more than desired number of times
		for (int i = 0; i < HitActors.Num(); i++) {
			if (OtherActor->GetName() == HitActors[i]) {
				return;
			}

		}
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), MeleeHit, GetActorLocation());
		FVector SpawnLocation = FireParticlesComp->GetComponentLocation();
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles, SpawnLocation, GetActorRotation(), true, EPSCPoolMethod::None);
		ManaComp->ResetSystem();		
		if (AttackType == Heavy) {
			if (DashBoost) {
				ExtendDash();
				UGameplayStatics::ApplyDamage(OtherActor, AttackPower * 1.5f * 1.5f, (AController*)GetController(), this, TSubclassOf<UDamageType>(UDamageType::StaticClass()));
			}
			else {
				UGameplayStatics::ApplyDamage(OtherActor, AttackPower * 1.5f, (AController*)GetController(), this, TSubclassOf<UDamageType>(UDamageType::StaticClass()));
			}
			RestoreMana(Soul * 1.25f);
		}
		else if (AttackType == Light) {
			if (DashBoost) {
				ExtendDash();
				UGameplayStatics::ApplyDamage(OtherActor, AttackPower * 1.5f, (AController*)GetController(), this, TSubclassOf<UDamageType>(UDamageType::StaticClass()));
			}
			else {
				UGameplayStatics::ApplyDamage(OtherActor, AttackPower, (AController*)GetController(), this, TSubclassOf<UDamageType>(UDamageType::StaticClass()));
			}
			RestoreMana(Soul);
		}
		HitActors.Add(OtherActor->GetName());		
	}
	else {
		//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Red, FString::Printf(TEXT("Actor Can't be Damaged")));
	}
}

void AActionPlayer::OnAttackOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	//GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Purple, "Ended Overlap!");
}
//If an enemy is close enough, add it to list of nearby enemies that can be locked onto via melee attack. Range is determined by a sphere component
void AActionPlayer::EnemyInRange(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {
	EnemyArray.Add(OtherActor);
}


void AActionPlayer::EnemyOutOfRange(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {
	EnemyArray.Remove(OtherActor);
}

void AActionPlayer::ResetAttackState() { //Reset all important variables
	AttackType = Nothing;
	CurrentTarget = NULL;
	TargetLocked = false;
	QueuedAttack = false;
	CurrentState = Normal;
	LightAttackIndex = 0;
	HeavyAttackIndex = 0;
}

void AActionPlayer::EnterAttack() {
	BreakTimer = true;
}

void AActionPlayer::ExitAttack() {
	ResetAttackState();
	//Reset rotation to normal because some of the mixamo root animations change rotation
	FRotator NewRotation = GetActorForwardVector().Rotation();
	NewRotation.Pitch *= 0;
	NewRotation.Roll *= 0;
	SetActorRotation(NewRotation);
}
//Catch all timer function
void AActionPlayer::BeginCountDown(FName Type){
	if (Type == "Attack") {
		TimeManager.AttackTimer = AttackCountdownTime;
		GetWorldTimerManager().SetTimer(TimeManager.AnimationHandle, this, &AActionPlayer::AdvanceAttackTimer, 0.1f, true);
	}
	else if (Type == "Invincibility") {
		TimeManager.InvincibilityTimer = InvincibilityCountdownTime;
		GetWorldTimerManager().SetTimer(TimeManager.InvincibilityHandle, this, &AActionPlayer::AdvanceInvincibilityTimer, 1.0f, true);
	}
	else if (Type == "Dash") {
		TimeManager.DashTimer = DashCountdownTime;
		GetWorldTimerManager().SetTimer(TimeManager.DashHandle, this, &AActionPlayer::AdvanceDashTimer, 0.1f, true);
	}
	else if (Type == "Heal") {
		TimeManager.HealTimer = HealCountdownTime;
		GetWorldTimerManager().SetTimer(TimeManager.HealHandle, this, &AActionPlayer::AdvanceHealTimer, 1.0f, true, 1.0f);
	}
}
//Various timer functions for variables that are time dependent
void AActionPlayer::AdvanceAttackTimer() { 
	TimeManager.AttackTimer -= 0.1f;
	if (TimeManager.AttackTimer < 0) {
		ResetAttackState();
	}
}

void AActionPlayer::AdvanceInvincibilityTimer() {
	--TimeManager.InvincibilityTimer;
	if (TimeManager.InvincibilityTimer < 1) {
		GetWorldTimerManager().ClearTimer(TimeManager.InvincibilityHandle); //We're done counting down, so stop running the timer
		Invincible = false;
	}
}

void AActionPlayer::AdvanceDashTimer() {
	TimeManager.DashTimer -= 0.1f;
	DashPercent = TimeManager.DashTimer / DashCountdownTime;
	if (TimeManager.DashTimer < 0) {
		MovementComponent->GroundFriction = BaseFriction;
		MovementComponent->MaxWalkSpeed = BaseWalkSpeed;		
		GetWorldTimerManager().ClearTimer(TimeManager.DashHandle);//We're done counting down, so stop running the timer
		CanDash = true;
		DashBoost = false;
		DashBoostParticles->Deactivate();
	}
}

void AActionPlayer::AdvanceHealTimer() {
	--TimeManager.HealTimer;
	if (TimeManager.HealTimer < 1) {

		TimeManager.HealTimer = HealCountdownTime;
		if (Mana >= HealRate) {
			LoseMana(HealRate);
			RestoreHealth(HealRate);
			if (HP == MaxHP) {
				StopHealing();
			}
		}
	}
}

void AActionPlayer::ExtendDash() {
	TimeManager.DashTimer += DashExtend;
	DashPercent = TimeManager.DashTimer / DashCountdownTime;
	if (TimeManager.DashTimer > DashCountdownTime) {
		TimeManager.DashTimer = DashCountdownTime;
	}
}