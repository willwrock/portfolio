// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GenericTeamAgentInterface.h"
#include "SkillsComponent.h"
#include "ActionPlayer.generated.h"


UENUM(BlueprintType)
enum FAttackType {
	Nothing UMETA(DisplayName = "Nothing"),
	Light UMETA(DisplayName = "Light"),
	Heavy UMETA(DisplayName = "Heavy"),
	Melee UMETA(DisplayName = "Melee"),
	Magic UMETA(DisplayName = "Magic")
};

UENUM(BlueprintType) 
enum FMoveDirection {
	Right UMETA(DisplayName = "Right"),
	Back UMETA(DisplayName = "Back"),
	Left UMETA(DisplayName = "Left"),
	Fore UMETA(DisplayName = "Fore")
};

UENUM(BlueprintType)
enum FState {
	Normal UMETA(DisplayName = "Normal"),
	Healing UMETA(DisplayName = "Healing"),
	InAir UMETA(DisplayName = "InAir"),
	Attacking UMETA(DisplayName = "Attacking"),
	InDodge UMETA(DisplayName = "InDodge"),
	Dead UMETA(DisplayName = "Dead"),
	Damaged UMETA(DisplayName = "Damaged")
};


USTRUCT(BlueprintType)
struct FPlayerActions : public FTableRowBase {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UAnimMontage* Montage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int32 AnimSectionCount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString Description;
};


USTRUCT(BlueprintType)
struct FMeleeCollisionProfile {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Enabled;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Disabled;

	//default constructor
	FMeleeCollisionProfile()
	{
		Enabled = FName(TEXT("Weapon"));
		Disabled = FName(TEXT("NoCollision"));
	}

	//FMeleeCollisionProfile()

};

USTRUCT(BlueprintType)
struct FCountDownType {

	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Attack;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Invincibility;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Dash;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName Heal;

	float AttackTimer;

	float InvincibilityTimer;

	float DashTimer;

	float HealTimer;

	FTimerHandle AnimationHandle; //For resetting the animation montage if player stops attacking

	FTimerHandle InvincibilityHandle; //For resetting the animation montage if player stops attacking

	FTimerHandle DashHandle; //For Cooldown on Dash move

	FTimerHandle HealHandle; //For Cooldown on Dash move

	FCountDownType() {
		Attack = FName(TEXT("Attack"));
		Invincibility = FName(TEXT("Invincibility"));
		Dash = FName(TEXT("Dash"));
		Heal = FName(TEXT("Heal"));
	}

};

UCLASS()
class ACTIONGAME_API AActionPlayer : public ACharacter, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AActionPlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Melee Attack Data Table
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
		class UDataTable* AttackDT;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* Staff;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Collision")
		class UBoxComponent* StaffCollision;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Collision")
		class USphereComponent* EnemyDetectionCollision;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UParticleSystem* ExplosionParticles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UParticleSystem* ImpactParticles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UParticleSystem* CraterParticles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UParticleSystemComponent* FireParticlesComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UNiagaraSystem* ManaParticles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UNiagaraComponent* ManaComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UParticleSystemComponent* DashBoostParticles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class UParticleSystemComponent* HealParticlesComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class URadialForceComponent* ExplosiveComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		class URadialForceComponent* CraterExplosion;


	FMeleeCollisionProfile MeleeCollisionProfile;

	FCountDownType TimeManager;

	UPROPERTY(VisibleAnywhere)
		class USpringArmComponent* SpringArm;

	UPROPERTY(VisibleAnywhere)
		class UCameraComponent* Camera;

	UPROPERTY(EditAnywhere)
	UCharacterMovementComponent* MovementComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		TArray <FSkillInfo> Skills;

	TArray <AActor*> IgnoredActors;

	virtual void Landed(const FHitResult& Hit) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float BaseTurnRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties")
		float DashPower;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties")
		float DoubleJumpPower;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Properties")
	float DodgePower;


	bool CanDash;

	bool EndOfCombo;

	bool DashBoost; 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float HP;

	UPROPERTY(EditAnywhere, Category = "Properties")
		float MaxHP;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float HealRate;

	UPROPERTY(VisibleAnywhere, Category = "Properties")
		float Mana;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float DashExtend; //How much does hitting an enemy extend the dash timer?

	UPROPERTY(EditAnywhere)
		float MaxMana;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float HealthPercent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float ManaPercent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float DashPercent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float AttackPower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float MagicPower;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf <class AFireball> FireballClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf <class AFireball> ExplosiveFireballClass;

	//How much Mana you regain upon htting an enemy
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float Soul;
	//How long are you invincible for after taking damage?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float InvincibilityTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float LightSpellCost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float HeavySpellCost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float DashSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Properties")
		float DashFriction;

	//How close can you be to an enemy to target it?
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		float TargetRadius;

	float BaseFriction;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 LightAttackIndex;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 HeavyAttackIndex;


	UPROPERTY(EditAnywhere)
		float AttackCountdownTime;

	UPROPERTY(EditAnywhere)
		float InvincibilityCountdownTime;

	UPROPERTY(EditAnywhere)
		float DashCountdownTime;

	UPROPERTY(EditAnywhere)
		float HealCountdownTime;

	//If you try to do an attack while another attack is in motion
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool QueuedAttack;

	bool TargetLocked;

	UPROPERTY(VisibleAnywhere)
		bool Invincible;

	UPROPERTY(BlueprintReadWrite)
		bool Dodging;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TEnumAsByte < FAttackType > AttackMode;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TEnumAsByte < FMoveDirection > DodgeDirection;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TEnumAsByte < FAttackType > AttackType;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TEnumAsByte < FState > CurrentState;

	UPROPERTY(EditAnywhere)
		USoundBase* ExplosionSound;

	UPROPERTY(EditAnywhere)
		USoundBase* CraterSound;

	UPROPERTY(EditAnywhere)
		USoundBase* MeleeHit;

	UPROPERTY(EditAnywhere)
		TArray<USoundBase*> DamageSounds;

	UPROPERTY(EditAnywhere)
		USoundBase* HealSound;

	//For overlap events that aren't supposed to hit something more than once
	TArray<FString> HitActors;

	int baseJumpVelocity;

	int BaseWalkSpeed;

	UPROPERTY(EditAnywhere)
		bool BreakTimer;

	UFUNCTION()
		void Dash();

	UFUNCTION()
		void Dodge();

	/** Called for forwards/backward input */
	UFUNCTION()
	void MoveForward(float Value);

	/** Called for side to side input */
	UFUNCTION()
	void MoveRight(float Value);

	UFUNCTION()
	void LookUpAtRate(float Rate);

	UFUNCTION()
	void TurnAtRate(float Rate);

	UFUNCTION()
	void StartJump();

	UFUNCTION()
	void StopJump();

	UFUNCTION()
	void StartSlowWalk();

	UFUNCTION()
	void StopSlowWalk();

	UFUNCTION()
	void LightAttackInput();

	UFUNCTION()
	void HeavyAttackInput();

	UFUNCTION(BlueprintCallable)
	void MeleeAttack();

	UFUNCTION(BlueprintCallable)
	void MeleeAttackEnd();

	UFUNCTION(BlueprintCallable)
		void MagicAttackStart();

	UFUNCTION(BlueprintCallable)
		void MagicAttackEnd();

	UFUNCTION(BlueprintCallable)
		void ExitAttack();

	UFUNCTION(BlueprintCallable)
		void EnterAttack();

	UFUNCTION()
		void StartHealing();

	UFUNCTION()
		void StopHealing();

	UFUNCTION()
		void NextAttack();

	UFUNCTION()
		void IncrementIndex(FAttackType Type);

	UFUNCTION()
		void SwitchMode();

	UFUNCTION(BlueprintCallable)
		void ResetAttackState();

	void QuitFunction();

	void Death();

	void FocusCamera();

	void ReloadLevel();

	class TArray<AActor*> EnemyArray;

	AActor* CurrentTarget;

	UFUNCTION()
		void FindTarget();

	UFUNCTION(BlueprintCallable)
		void TargetFocus();

	UFUNCTION()
		void TargetRelease();

	//UFUNCTION()
		//void Quit();

	UFUNCTION()
		void EnemyInRange(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void EnemyOutOfRange(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION() //Make delegate related functions UFUNCTIONS
	void OnAttackHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
		void OnAttackOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnAttackOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void LoseHealth(float amount);

	UFUNCTION()
	void LoseMana(float amount);

	UFUNCTION()
		void RestoreHealth(float amount);

	UFUNCTION()
		void RestoreMana(float amount);

	UFUNCTION()
		void BeginCountDown(FName Type);

	UFUNCTION()
		void AdvanceAttackTimer();

	UFUNCTION()
		void AdvanceInvincibilityTimer();

	UFUNCTION()
		void AdvanceDashTimer();

	UFUNCTION()
		void AdvanceHealTimer();

	UFUNCTION()
		void SummonMagic(UClass* Class);

	UFUNCTION()
	void Explosion();

	UFUNCTION()
		void Fireball();

	UFUNCTION()
		void Grenade();

	UFUNCTION()
		void SummonCrater();

	void ExtendDash();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	FPlayerActions* DamageMontage;

	FGenericTeamId TeamId;

	virtual FGenericTeamId GetGenericTeamId() const override { return TeamId; }
	
};
